<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digislides'][$id]['reponse'])) {
		$reponse = $_SESSION['digislides'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digislides_presentations WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($presentation = $stmt->fetchAll()) {
			$admin = false;
			if (count($presentation, COUNT_NORMAL) > 0 && $presentation[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $presentation[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digislides'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digislides'][$id]['digidrive'];
			} else if (intval($presentation[0]['digidrive']) === 1) {
				$digidrive = 1;
			}
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($presentation[0]['vues'] !== '') {
				$vues = intval($presentation[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			$stmt = $db->prepare('UPDATE digislides_presentations SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
			if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
				echo json_encode(array('nom' => $presentation[0]['nom'], 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
