<?php

session_start();

require 'headers.php';

if (!empty($_POST['presentation']) && !empty($_POST['nouveaunom'])) {
	require 'db.php';
	$reponse = '';
	$presentation = $_POST['presentation'];
	if (isset($_SESSION['digislides'][$presentation]['reponse'])) {
		$reponse = $_SESSION['digislides'][$presentation]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digislides_presentations WHERE url = :url');
	if ($stmt->execute(array('url' => $presentation))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$nouveaunom = $_POST['nouveaunom'];
			$stmt = $db->prepare('UPDATE digislides_presentations SET nom = :nouveaunom WHERE url = :url');
			if ($stmt->execute(array('nouveaunom' => $nouveaunom, 'url' => $presentation))) {
				echo 'nom_modifie';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
