import { createRouter, createWebHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Presentation from '../views/Presentation.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/s/:id',
		name: 'Presentation',
		component: Presentation
	}
]

let router
if (import.meta.env.VITE_FOLDER) {
	router = createRouter({
		history: createWebHistory(import.meta.env.VITE_FOLDER),
		routes
	})
} else {
	router = createRouter({
		history: createWebHistory(),
		routes
	})
}

export default router
